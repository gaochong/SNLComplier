package main;

import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;

import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;

import lex.Scanner;
import parser.DrawTree;
import parser.DrawTreeDialog;
import parser.Parser;
import parser.RecursiveDescent;
import semantic.SemanticAnalysis;
import common.Node;
import common.TreeNode;

public class MainFrame extends JFrame {
	private static final long serialVersionUID = 3326520867223159241L;

	private JTextField tfFile = new JTextField(20);
	private JButton bt0 = new JButton("打开文件");
	private JButton bt1 = new JButton("词法分析");
	private JButton bt2 = new JButton("递归下降");
	private JButton bt3 = new JButton("LL1语法分析");
	private JButton bt4 = new JButton("语义分析");
	private JTextArea results = new JTextArea();
	private String filename = "";
	private String s = "";

	public void print(String s) {
		results.setText("");
		results.append(s + "\n");
	}

	public MainFrame() {
		JPanel p = new JPanel();
		p.setLayout(new FlowLayout());
		p.add(new JLabel("filename:"));
		p.add(tfFile);
		p.add(bt0);
		bt0.addActionListener(new OpenL());
		p.add(bt1);
		bt1.addActionListener(new LexL());
		p.add(bt2);
		bt2.addActionListener(new RecL());
		p.add(bt3);
		bt3.addActionListener(new LL1L());
		p.add(bt4);
		bt4.addActionListener(new SemL());
		add(BorderLayout.NORTH, p);

		add(new JScrollPane(results));

	}

	class OpenL implements ActionListener {
		public void actionPerformed(ActionEvent e) {
			JFileChooser c = new JFileChooser();
			// Demonstrate "Open" dialog:
			int rVal = c.showOpenDialog(MainFrame.this);
			if (rVal == JFileChooser.APPROVE_OPTION) {
				try {
					filename = c.getCurrentDirectory().toString() + "\\"
							+ c.getSelectedFile().getName();
					tfFile.setText(filename);
					
					s = "";
					FileReader file = new FileReader(filename);
					BufferedReader reader = new BufferedReader(file);
					while (reader.ready()) {
						s += reader.readLine();
						s += '\n';
					}
					// 关闭reader
					reader.close();
					print(s);
				} catch (FileNotFoundException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				} catch (IOException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
				
			}
			if (rVal == JFileChooser.CANCEL_OPTION) {
				tfFile.setText("You pressed cancel");
				filename = "";
			}
		}
	}
	
	
	class LexL implements ActionListener {
		public void actionPerformed(ActionEvent e) {
			if ("".equals(filename)) {
				tfFile.setText("no file");
			} else {
				
				try {
					//词法分析
					Scanner scanner = new Scanner();
					ArrayList<Node> tokenList;
					tokenList = scanner.getTokenList(filename);
					s = "";
					for (Node node : tokenList) {
						System.out.println(node.getData() + "   " + node.getType());
						s += node.getLine()+ "\t" + node.getData() + "\t" + node.getType() + "\n";
					}
					print(s);
				} catch (Exception e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
				
			}
		}
	}
	class RecL implements ActionListener {
		public void actionPerformed(ActionEvent e) {
			if ("".equals(filename)) {
				tfFile.setText("no file");
			} else {
				
				try {
					TreeNode root = new TreeNode();

					RecursiveDescent parser = new RecursiveDescent(filename);
					

					root = parser.program();
					if (root != null) {
						DrawTreeDialog dtDlg = new DrawTreeDialog(null, root);
						dtDlg.setVisible(true);
					}
				} catch (Exception e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
				
			}
		}
	}

	
	class LL1L implements ActionListener {
		public void actionPerformed(ActionEvent e) {
			if ("".equals(filename)) {
				tfFile.setText("no file");
			} else {
				
				try {
					TreeNode root = new TreeNode();
					
					Parser parser = new Parser();
					root = parser.getTree1(filename);
					
					if (root != null) {
						DrawTreeDialog dtDlg = new DrawTreeDialog(null, root);
						dtDlg.setVisible(true);
					}
				} catch (Exception e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
				
			}
		}
	}
	class SemL implements ActionListener {
		public void actionPerformed(ActionEvent e) {
			if ("".equals(filename)) {
				tfFile.setText("no file");
			} else {
				
				try {
					TreeNode root = new TreeNode();

					Parser parser = new Parser();
					root = parser.getTree1(filename);

					SemanticAnalysis sa = new SemanticAnalysis();

					sa.Program(root);

					
					s = "恭喜!您的程序无语义错误!\n";
					s += sa.getStringIdTable();
					print(s);
				} catch (Exception e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
				
			}
		}
	}

	public static void main(String[] args) {
		MainFrame f = new MainFrame();
		f.setTitle("编译器");
		f.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		f.setSize(800, 600);
		f.setVisible(true);

	}
} // /:~