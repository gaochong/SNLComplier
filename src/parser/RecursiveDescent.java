package parser;

import java.util.ArrayList;

import lex.Scanner;
import common.Enum;
import common.Node;
import common.SNLPredict;
import common.TreeNode;

/***
 * 递归下降法
 * 
 * @author zf
 */
public class RecursiveDescent {
	/** 得到的所有的token序列 */
	private ArrayList<Node> nodeList = new ArrayList<Node>();
	/** 所有的产生式的predict集 */
	private SNLPredict Predict = new SNLPredict();
	/** token的下标 */
	private int token_index;

	public RecursiveDescent() {
		String path = System.getProperty("user.dir");
		path = path.replace("\\", "/");
		token_index = 0;
		try {
			Scanner scanner = new Scanner();
			this.nodeList = scanner.getTokenList(path + "/src/c11.snl");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	public RecursiveDescent(String path) {
		token_index = 0;
		try {
			Scanner scanner = new Scanner();
			this.nodeList = scanner.getTokenList(path);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	// 1 =1
	public TreeNode program() {
		TreeNode ROOT = new TreeNode();
		ROOT.setData("Program");
		ROOT.setflag(1);
		ROOT.setNonTerminal(Enum.nonTerminals.Program);

		for (int i = 0; i < Predict.predict[1].getPredictNum(); i++) {
			if (nodeList.get(token_index).getType()
					.equals(Predict.predict[1].getPredict(i))) {
				TreeNode programHead = programHead();
				TreeNode declarePart = declarePart();
				TreeNode programBody = programBody();

				ROOT.setChild(programHead);
				ROOT.setChild(declarePart);
				ROOT.setChild(programBody);
				programHead.setFather(ROOT);
				declarePart.setFather(ROOT);
				programBody.setFather(ROOT);

				return ROOT;
			}
		}
		syntaxError();
		return null;
	}

	// 1-1 =2
	private TreeNode programHead() {
		TreeNode programHead = new TreeNode();
		programHead.setData("ProgramHead");
		programHead.setflag(1);
		programHead.setNonTerminal(Enum.nonTerminals.ProgramHead);

		for (int i = 0; i < Predict.predict[2].getPredictNum(); i++) {
			if (nodeList.get(token_index).getType()
					.equals(Predict.predict[2].getPredict(i))) {
				match("PROGRAM");
				TreeNode programName = programName();

				TreeNode PROGRAM = new TreeNode();
				PROGRAM.setData("PROGRAM");
				PROGRAM.setflag(0);
				PROGRAM.setLength(nodeList.get(token_index).getLine());

				programHead.setChild(PROGRAM);
				programHead.setChild(programName);
				PROGRAM.setFather(programHead);
				programName.setFather(programHead);
				return programHead;
			}
		}
		syntaxError();
		return null;

	}

	// 1-2 =3
	private TreeNode declarePart() {
		TreeNode declarePart = new TreeNode();
		declarePart.setData("declarePart");
		declarePart.setflag(1);
		declarePart.setNonTerminal(Enum.nonTerminals.DeclarePart);

		for (int i = 0; i < Predict.predict[4].getPredictNum(); i++) {
			if (nodeList.get(token_index).getType()
					.equals(Predict.predict[4].getPredict(i))) {
				TreeNode typeDecpart = typeDecpart();
				TreeNode varDecpart = varDecpart();
				TreeNode procDecpart = procDecpart();

				declarePart.setChild(typeDecpart);
				declarePart.setChild(varDecpart);
				declarePart.setChild(procDecpart);
				typeDecpart.setFather(declarePart);
				varDecpart.setFather(declarePart);
				procDecpart.setFather(declarePart);

				return declarePart;
			}
		}
		syntaxError();
		return null;
	}

	// 1-3 =4
	public TreeNode programBody() {
		TreeNode programBody = new TreeNode();
		programBody.setData("programBody");
		programBody.setflag(1);
		programBody.setNonTerminal(Enum.nonTerminals.ProgramBody);

		for (int i = 0; i < Predict.predict[57].getPredictNum(); i++) {
			if (nodeList.get(token_index).getType()
					.equals(Predict.predict[57].getPredict(i))) {
				match("BEGIN");
				TreeNode stmList = stmList();
				match("END");

				TreeNode BEGIN = new TreeNode();
				BEGIN.setData("BEGIN");
				BEGIN.setflag(0);
				BEGIN.setLength(nodeList.get(token_index).getLine());

				TreeNode END = new TreeNode();
				END.setData("END");
				END.setflag(0);
				END.setLength(nodeList.get(token_index).getLine());

				programBody.setChild(stmList);
				programBody.setChild(END);
				programBody.setChild(BEGIN);
				stmList.setFather(programBody);
				END.setFather(programBody);
				BEGIN.setFather(programBody);

				return programBody;
			}
		}
		syntaxError();
		return null;
	}

	// 2-1=5
	public TreeNode programName() {
		TreeNode programName = new TreeNode();
		programName.setData("programName");
		programName.setflag(1);
		programName.setNonTerminal(Enum.nonTerminals.ProgramName);

		for (int i = 0; i < Predict.predict[3].getPredictNum(); i++) {
			if (nodeList.get(token_index).getType()
					.equals(Predict.predict[3].getPredict(i))) {
				match("ID");

				TreeNode ID = new TreeNode();
				ID.setData("ID");
				ID.setflag(0);
				ID.setLength(nodeList.get(token_index).getLine());
				
				programName.setChild(ID);
				ID.setFather(programName);

				return programName;
			}
		}
		syntaxError();
		return null;
	}

	// 4-1=6
	private TreeNode typeDecpart() {
		TreeNode typeDecpart = new TreeNode();
		typeDecpart.setData("typeDecpart");
		typeDecpart.setflag(1);
		typeDecpart.setNonTerminal(Enum.nonTerminals.TypeDecpart);

		for (int i = 0; i < Predict.predict[5].getPredictNum(); i++) {
			if (nodeList.get(token_index).getType()
					.equals(Predict.predict[5].getPredict(i))) {
				return typeDecpart;
			}
		}
		for (int i = 0; i < Predict.predict[6].getPredictNum(); i++) {
			if (nodeList.get(token_index).getType()
					.equals(Predict.predict[6].getPredict(i))) {
				TreeNode typeDec = typeDec();

				typeDecpart.setChild(typeDec);
				typeDec.setFather(typeDecpart);

				return typeDecpart;
			}
		}
		syntaxError();
		return null;
	}

	// 4-2=7
	private TreeNode varDecpart() {
		TreeNode varDecpart = new TreeNode();
		varDecpart.setData("varDecpart");
		varDecpart.setflag(1);
		varDecpart.setNonTerminal(Enum.nonTerminals.VarDecpart);

		for (int i = 0; i < Predict.predict[30].getPredictNum(); i++) {
			if (nodeList.get(token_index).getType()
					.equals(Predict.predict[30].getPredict(i))) {
				return varDecpart;
			}
		}
		for (int i = 0; i < Predict.predict[31].getPredictNum(); i++) {
			if (nodeList.get(token_index).getType()
					.equals(Predict.predict[31].getPredict(i))) {
				TreeNode varDec = varDec();

				varDecpart.setChild(varDec);
				varDec.setFather(varDecpart);

				return varDecpart;
			}
		}
		syntaxError();
		return null;
	}

	// 4-3=8
	private TreeNode procDecpart() {
		TreeNode procDecpart = new TreeNode();
		procDecpart.setData("procDecpart");
		procDecpart.setflag(1);
		procDecpart.setNonTerminal(Enum.nonTerminals.ProcDecpart);

		for (int i = 0; i < Predict.predict[39].getPredictNum(); i++) {
			if (nodeList.get(token_index).getType()
					.equals(Predict.predict[39].getPredict(i))) {
				return procDecpart;
			}
		}
		for (int i = 0; i < Predict.predict[40].getPredictNum(); i++) {
			if (nodeList.get(token_index).getType()
					.equals(Predict.predict[40].getPredict(i))) {
				TreeNode procDec = procDec();

				procDecpart.setChild(procDec);
				procDec.setFather(procDecpart);

				return procDecpart;
			}
		}
		syntaxError();
		return null;
	}

	// 6-1=9
	private TreeNode typeDec() {
		TreeNode typeDec = new TreeNode();
		typeDec.setData("typeDec");
		typeDec.setflag(1);
		typeDec.setNonTerminal(Enum.nonTerminals.TypeDec);

		for (int i = 0; i < Predict.predict[7].getPredictNum(); i++) {
			if (nodeList.get(token_index).getType()
					.equals(Predict.predict[7].getPredict(i))) {
				match("TYPE");
				TreeNode typeDeclist = typeDeclist();

				TreeNode TYPE = new TreeNode();
				TYPE.setData("TYPE");
				TYPE.setflag(0);
				TYPE.setLength(nodeList.get(token_index).getLine());

				typeDec.setChild(typeDeclist);
				typeDec.setChild(TYPE);
				typeDeclist.setFather(typeDec);
				TYPE.setFather(typeDec);
				return typeDec;
			}
		}
		syntaxError();
		return null;
	}

	// 7-1=10
	private TreeNode typeDeclist() {
		TreeNode typeDeclist = new TreeNode();
		typeDeclist.setData("typeDeclist");
		typeDeclist.setflag(1);
		typeDeclist.setNonTerminal(Enum.nonTerminals.TypeDecList);

		for (int i = 0; i < Predict.predict[8].getPredictNum(); i++) {
			if (nodeList.get(token_index).getType()
					.equals(Predict.predict[8].getPredict(i))) {
				TreeNode typeId = typeId();
				match("=");
				TreeNode typeDef = typeDef();
				match(";");
				TreeNode typeDecmore = typeDecmore();

				TreeNode E = new TreeNode();
				E.setData("=");
				E.setflag(0);
				E.setLength(nodeList.get(token_index).getLine());
				TreeNode SEMI = new TreeNode();
				SEMI.setData(";");
				SEMI.setflag(0);
				SEMI.setLength(nodeList.get(token_index).getLine());

				typeDeclist.setChild(typeId);
				typeDeclist.setChild(E);
				typeDeclist.setChild(typeDef);
				typeDeclist.setChild(SEMI);
				typeDeclist.setChild(typeDecmore);

				typeId.setFather(typeDeclist);
				E.setFather(typeDeclist);
				typeDef.setFather(typeDeclist);
				SEMI.setFather(typeDeclist);
				typeDecmore.setFather(typeDeclist);

				return typeDeclist;
			}
		}
		syntaxError();
		return null;
	}

	// 8-1=11
	private TreeNode typeId() {
		TreeNode typeId = new TreeNode();
		typeId.setData("typeId");
		typeId.setflag(1);
		typeId.setNonTerminal(Enum.nonTerminals.TypeId);

		for (int i = 0; i < Predict.predict[11].getPredictNum(); i++) {
			if (nodeList.get(token_index).getType()
					.equals(Predict.predict[11].getPredict(i))) {
				match("ID");
				TreeNode ID = new TreeNode();
				ID.setData("ID");
				ID.setflag(0);
				ID.setLength(nodeList.get(token_index).getLine());

				typeId.setChild(ID);
				ID.setFather(typeId);

				return typeId;
			}
		}
		syntaxError();
		return null;
	}

	// 8-2=12
	private TreeNode typeDef() {
		TreeNode typeDef = new TreeNode();
		typeDef.setData("typeDef");
		typeDef.setflag(1);
		typeDef.setNonTerminal(Enum.nonTerminals.TypeDef);

		for (int i = 0; i < Predict.predict[12].getPredictNum(); i++) {
			if (nodeList.get(token_index).getType()
					.equals(Predict.predict[12].getPredict(i))) {
				TreeNode baseType = baseType();

				typeDef.setChild(baseType);
				baseType.setFather(typeDef);

				return typeDef;
			}
		}
		for (int i = 0; i < Predict.predict[13].getPredictNum(); i++) {
			if (nodeList.get(token_index).getType()
					.equals(Predict.predict[13].getPredict(i))) {
				TreeNode structureType = structureType();

				typeDef.setChild(structureType);
				structureType.setFather(typeDef);

				return typeDef;
			}
		}
		for (int i = 0; i < Predict.predict[14].getPredictNum(); i++) {
			if (nodeList.get(token_index).getType()
					.equals(Predict.predict[14].getPredict(i))) {
				match("ID");

				TreeNode ID = new TreeNode();
				ID.setData("ID");
				ID.setflag(0);
				ID.setLength(nodeList.get(token_index).getLine());

				typeDef.setChild(ID);
				ID.setFather(typeDef);

				return typeDef;
			}
		}
		syntaxError();
		return null;
	}

	// 8-3=13
	private TreeNode typeDecmore() {
		TreeNode typeDecmore = new TreeNode();
		typeDecmore.setData("typeDecmore");
		typeDecmore.setflag(1);
		typeDecmore.setNonTerminal(Enum.nonTerminals.TypeDecMore);

		for (int i = 0; i < Predict.predict[9].getPredictNum(); i++) {
			if (nodeList.get(token_index).getType()
					.equals(Predict.predict[9].getPredict(i))) {
				return typeDecmore;
			}
		}
		for (int i = 0; i < Predict.predict[10].getPredictNum(); i++) {
			if (nodeList.get(token_index).getType()
					.equals(Predict.predict[10].getPredict(i))) {
				TreeNode typeDeclist = typeDeclist();

				typeDecmore.setChild(typeDeclist);
				typeDeclist.setFather(typeDecmore);

				return typeDecmore;
			}
		}
		syntaxError();
		return null;
	}

	// 12-1=14
	private TreeNode baseType() {
		TreeNode baseType = new TreeNode();
		baseType.setData("baseType");
		baseType.setflag(1);
		baseType.setNonTerminal(Enum.nonTerminals.BaseType);

		for (int i = 0; i < Predict.predict[15].getPredictNum(); i++) {
			if (nodeList.get(token_index).getType()
					.equals(Predict.predict[15].getPredict(i))) {
				match("INTEGER");
				TreeNode INTEGER = new TreeNode();
				INTEGER.setData("INTEGER");
				INTEGER.setflag(0);
				INTEGER.setLength(nodeList.get(token_index).getLine());

				baseType.setChild(INTEGER);
				INTEGER.setFather(baseType);

				return baseType;
			}
		}
		for (int i = 0; i < Predict.predict[16].getPredictNum(); i++) {
			if (nodeList.get(token_index).getType()
					.equals(Predict.predict[16].getPredict(i))) {
				match("CHAR");
				TreeNode CHAR = new TreeNode();
				CHAR.setData("CHAR");
				CHAR.setflag(0);
				CHAR.setLength(nodeList.get(token_index).getLine());

				baseType.setChild(CHAR);
				CHAR.setFather(baseType);

				return baseType;
			}
		}
		syntaxError();
		return null;
	}

	// 12-2=15
	private TreeNode structureType() {
		TreeNode structureType = new TreeNode();
		structureType.setData("structureType");
		structureType.setflag(1);
		structureType.setNonTerminal(Enum.nonTerminals.StructureType);

		for (int i = 0; i < Predict.predict[17].getPredictNum(); i++) {
			if (nodeList.get(token_index).getType()
					.equals(Predict.predict[17].getPredict(i))) {
				TreeNode arrayType = arrayType();

				structureType.setChild(arrayType);
				arrayType.setFather(structureType);

				return structureType;
			}
		}
		for (int i = 0; i < Predict.predict[18].getPredictNum(); i++) {
			if (nodeList.get(token_index).getType()
					.equals(Predict.predict[18].getPredict(i))) {
				TreeNode recType = recType();

				structureType.setChild(recType);
				recType.setFather(structureType);

				return structureType;
			}
		}
		syntaxError();
		return null;
	}

	// 17-1=16
	private TreeNode arrayType() {
		TreeNode arrayType = new TreeNode();
		arrayType.setData("arrayType");
		arrayType.setflag(1);
		arrayType.setNonTerminal(Enum.nonTerminals.ArrayType);

		for (int i = 0; i < Predict.predict[19].getPredictNum(); i++) {
			if (nodeList.get(token_index).getType()
					.equals(Predict.predict[19].getPredict(i))) {
				match("ARRAY");
				match("[");
				TreeNode low = low();
				match("..");
				TreeNode top = top();
				match("]");
				match("OF");
				TreeNode baseType = baseType();

				TreeNode ARRAY = new TreeNode();
				ARRAY.setData("ARRAY");
				ARRAY.setflag(0);
				ARRAY.setLength(nodeList.get(token_index).getLine());
				TreeNode Z = new TreeNode();
				Z.setData("[");
				Z.setflag(0);
				Z.setLength(nodeList.get(token_index).getLine());
				TreeNode D = new TreeNode();
				D.setData("..");
				D.setflag(0);
				D.setLength(nodeList.get(token_index).getLine());
				TreeNode Y = new TreeNode();
				Y.setData("]");
				Y.setflag(0);
				Y.setLength(nodeList.get(token_index).getLine());
				TreeNode OF = new TreeNode();
				OF.setData("OF");
				OF.setflag(0);
				OF.setLength(nodeList.get(token_index).getLine());

				arrayType.setChild(ARRAY);
				arrayType.setChild(Z);
				arrayType.setChild(low);
				arrayType.setChild(D);
				arrayType.setChild(top);
				arrayType.setChild(Y);
				arrayType.setChild(OF);
				arrayType.setChild(baseType);

				ARRAY.setFather(arrayType);
				Z.setFather(arrayType);
				low.setFather(arrayType);
				D.setFather(arrayType);
				top.setFather(arrayType);
				Y.setFather(arrayType);
				OF.setFather(arrayType);
				baseType.setFather(arrayType);

				return arrayType;
			}
		}
		syntaxError();
		return null;
	}

	// 17-2=17
	private TreeNode recType() {
		TreeNode recType = new TreeNode();
		recType.setData("recType");
		recType.setflag(1);
		recType.setNonTerminal(Enum.nonTerminals.RecType);

		for (int i = 0; i < Predict.predict[22].getPredictNum(); i++) {
			if (nodeList.get(token_index).getType()
					.equals(Predict.predict[22].getPredict(i))) {
				match("RECORD");
				TreeNode fieldDeclist = fieldDeclist();
				match("END");

				TreeNode RECORD = new TreeNode();
				RECORD.setData("RECORD");
				RECORD.setflag(0);
				RECORD.setLength(nodeList.get(token_index).getLine());
				TreeNode END = new TreeNode();
				END.setData("END");
				END.setflag(0);
				END.setLength(nodeList.get(token_index).getLine());
				
				recType.setChild(RECORD);
				recType.setChild(fieldDeclist);
				recType.setChild(END);

				RECORD.setFather(recType);
				fieldDeclist.setFather(recType);
				END.setFather(recType);

				return recType;
			}
		}
		syntaxError();
		return null;
	}

	// 19-1=18
	private TreeNode low() {
		TreeNode low = new TreeNode();
		low.setData("low");
		low.setflag(1);
		low.setNonTerminal(Enum.nonTerminals.Low);

		for (int i = 0; i < Predict.predict[20].getPredictNum(); i++) {
			if (nodeList.get(token_index).getType()
					.equals(Predict.predict[20].getPredict(i))) {
				match("INTC");

				TreeNode INTC = new TreeNode();
				INTC.setData("INTC");
				INTC.setflag(0);
				INTC.setLength(nodeList.get(token_index).getLine());
				
				low.setChild(INTC);

				INTC.setFather(low);

				return low;
			}
		}
		syntaxError();
		return null;
	}

	// 19-2=19
	private TreeNode top() {
		TreeNode top = new TreeNode();
		top.setData("top");
		top.setflag(1);
		top.setNonTerminal(Enum.nonTerminals.Top);

		for (int i = 0; i < Predict.predict[21].getPredictNum(); i++) {
			if (nodeList.get(token_index).getType()
					.equals(Predict.predict[21].getPredict(i))) {
				match("INTC");

				TreeNode INTC = new TreeNode();
				INTC.setData("INTC");
				INTC.setflag(0);
				INTC.setLength(nodeList.get(token_index).getLine());

				top.setChild(INTC);

				INTC.setFather(top);

				return top;
			}
		}
		syntaxError();
		return null;
	}

	// 22-1=20
	private TreeNode fieldDeclist() {
		TreeNode fieldDeclist = new TreeNode();
		fieldDeclist.setData("fieldDeclist");
		fieldDeclist.setflag(1);
		fieldDeclist.setNonTerminal(Enum.nonTerminals.FieldDecList);

		for (int i = 0; i < Predict.predict[23].getPredictNum(); i++) {
			if (nodeList.get(token_index).getType()
					.equals(Predict.predict[23].getPredict(i))) {
				TreeNode baseType = baseType();
				TreeNode idList = idList();
				match(";");
				TreeNode fieldDecmore = fieldDecmore();

				TreeNode SEMI = new TreeNode();
				SEMI.setData(";");
				SEMI.setflag(0);
				SEMI.setLength(nodeList.get(token_index).getLine());
				
				fieldDeclist.setChild(baseType);
				fieldDeclist.setChild(idList);
				fieldDeclist.setChild(SEMI);
				fieldDeclist.setChild(fieldDecmore);

				baseType.setFather(fieldDeclist);
				idList.setFather(fieldDeclist);
				SEMI.setFather(fieldDeclist);
				fieldDecmore.setFather(fieldDeclist);

				return fieldDeclist;
			}
		}
		for (int i = 0; i < Predict.predict[24].getPredictNum(); i++) {
			if (nodeList.get(token_index).getType()
					.equals(Predict.predict[24].getPredict(i))) {
				TreeNode arrayType = arrayType();
				TreeNode idList = idList();
				match(";");
				TreeNode fieldDecmore = fieldDecmore();

				TreeNode SEMI = new TreeNode();
				SEMI.setData(";");
				SEMI.setflag(0);
				SEMI.setLength(nodeList.get(token_index).getLine());

				fieldDeclist.setChild(arrayType);
				fieldDeclist.setChild(idList);
				fieldDeclist.setChild(SEMI);
				fieldDeclist.setChild(fieldDecmore);

				arrayType.setFather(fieldDeclist);
				idList.setFather(fieldDeclist);
				SEMI.setFather(fieldDeclist);
				fieldDecmore.setFather(fieldDeclist);

				return fieldDeclist;
			}
		}
		syntaxError();
		return null;
	}

	// 23-1=21
	private TreeNode idList() {
		TreeNode idList = new TreeNode();
		idList.setData("idList");
		idList.setflag(1);
		idList.setNonTerminal(Enum.nonTerminals.IdList);

		for (int i = 0; i < Predict.predict[27].getPredictNum(); i++) {
			if (nodeList.get(token_index).getType()
					.equals(Predict.predict[27].getPredict(i))) {
				match("ID");
				TreeNode idMore = idMore();

				TreeNode ID = new TreeNode();
				ID.setData("ID");
				ID.setflag(0);
				ID.setLength(nodeList.get(token_index).getLine());

				idList.setChild(ID);
				idList.setChild(idMore);

				ID.setFather(idList);
				idMore.setFather(idList);

				return idList;
			}
		}
		syntaxError();
		return null;
	}

	// 23-2=22
	private TreeNode fieldDecmore() {
		TreeNode fieldDecmore = new TreeNode();
		fieldDecmore.setData("fieldDecmore");
		fieldDecmore.setflag(1);
		fieldDecmore.setNonTerminal(Enum.nonTerminals.FieldDecMore);

		for (int i = 0; i < Predict.predict[25].getPredictNum(); i++) {
			if (nodeList.get(token_index).getType()
					.equals(Predict.predict[25].getPredict(i))) {
				return fieldDecmore;
			}
		}
		for (int i = 0; i < Predict.predict[26].getPredictNum(); i++) {
			if (nodeList.get(token_index).getType()
					.equals(Predict.predict[26].getPredict(i))) {
				TreeNode fieldDeclist = fieldDeclist();

				fieldDecmore.setChild(fieldDeclist);

				fieldDeclist.setFather(fieldDecmore);

				return fieldDecmore;
			}
		}
		syntaxError();
		return null;
	}

	// 27-1=23
	private TreeNode idMore() {
		TreeNode idMore = new TreeNode();
		idMore.setData("idMore");
		idMore.setflag(1);
		idMore.setNonTerminal(Enum.nonTerminals.IdMore);

		for (int i = 0; i < Predict.predict[28].getPredictNum(); i++) {
			if (nodeList.get(token_index).getType()
					.equals(Predict.predict[28].getPredict(i))) {
				return idMore;
			}
		}
		for (int i = 0; i < Predict.predict[29].getPredictNum(); i++) {
			if (nodeList.get(token_index).getType()
					.equals(Predict.predict[29].getPredict(i))) {
				match(",");
				TreeNode idList = idList();

				TreeNode SEMI = new TreeNode();
				SEMI.setData(";");
				SEMI.setflag(0);
				SEMI.setLength(nodeList.get(token_index).getLine());

				idMore.setChild(SEMI);
				idMore.setChild(idList);

				SEMI.setFather(idMore);
				idList.setFather(idMore);

				return idMore;
			}
		}
		syntaxError();
		return null;
	}

	// 30-1=24
	private TreeNode varDec() {
		TreeNode varDec = new TreeNode();
		varDec.setData("varDec");
		varDec.setflag(1);
		varDec.setNonTerminal(Enum.nonTerminals.VarDec);

		for (int i = 0; i < Predict.predict[32].getPredictNum(); i++) {
			if (nodeList.get(token_index).getType()
					.equals(Predict.predict[32].getPredict(i))) {
				match("VAR");
				TreeNode varDeclist = varDeclist();

				TreeNode VAR = new TreeNode();
				VAR.setData("VAR");
				VAR.setflag(0);
				VAR.setLength(nodeList.get(token_index).getLine());

				varDec.setChild(VAR);
				varDec.setChild(varDeclist);

				VAR.setFather(varDec);
				varDeclist.setFather(varDec);

				return varDec;
			}
		}
		syntaxError();
		return null;
	}

	// 32-1=25
	private TreeNode varDeclist() {
		TreeNode varDeclist = new TreeNode();
		varDeclist.setData("varDeclist");
		varDeclist.setflag(1);
		varDeclist.setNonTerminal(Enum.nonTerminals.VarDecList);

		for (int i = 0; i < Predict.predict[33].getPredictNum(); i++) {
			if (nodeList.get(token_index).getType()
					.equals(Predict.predict[33].getPredict(i))) {
				TreeNode typeDef = typeDef();
				TreeNode varIdlist = varIdlist();
				match(";");
				TreeNode varDecmore = varDecmore();

				TreeNode SEMI = new TreeNode();
				SEMI.setData("SEMI");
				SEMI.setflag(0);
				SEMI.setLength(nodeList.get(token_index).getLine());

				varDeclist.setChild(typeDef);
				varDeclist.setChild(varIdlist);
				varDeclist.setChild(SEMI);
				varDeclist.setChild(varDecmore);

				typeDef.setFather(varDeclist);
				varIdlist.setFather(varDeclist);
				SEMI.setFather(varDeclist);
				varDecmore.setFather(varDeclist);

				return varDeclist;
			}
		}
		syntaxError();
		return null;
	}

	// 33-2=26
	private TreeNode varIdlist() {
		TreeNode varIdlist = new TreeNode();
		varIdlist.setData("varIdlist");
		varIdlist.setflag(1);
		varIdlist.setNonTerminal(Enum.nonTerminals.VarIdList);

		for (int i = 0; i < Predict.predict[36].getPredictNum(); i++) {
			if (nodeList.get(token_index).getType()
					.equals(Predict.predict[36].getPredict(i))) {
				match("ID");
				TreeNode varIdmore = varIdmore();

				TreeNode ID = new TreeNode();
				ID.setData("ID");
				ID.setflag(0);
				ID.setLength(nodeList.get(token_index).getLine());
				
				varIdlist.setChild(ID);
				varIdlist.setChild(varIdmore);

				ID.setFather(varIdlist);
				varIdmore.setFather(varIdlist);

				return varIdlist;
			}
		}
		syntaxError();
		return null;
	}

	// 33-3=27
	private TreeNode varDecmore() {
		TreeNode varDecmore = new TreeNode();
		varDecmore.setData("varDecmore");
		varDecmore.setflag(1);
		varDecmore.setNonTerminal(Enum.nonTerminals.VarDecMore);

		for (int i = 0; i < Predict.predict[34].getPredictNum(); i++) {
			if (nodeList.get(token_index).getType()
					.equals(Predict.predict[34].getPredict(i))) {
				return varDecmore;
			}
		}
		for (int i = 0; i < Predict.predict[35].getPredictNum(); i++) {
			if (nodeList.get(token_index).getType()
					.equals(Predict.predict[35].getPredict(i))) {
				TreeNode varDeclist = varDeclist();

				varDecmore.setChild(varDeclist);

				varDeclist.setFather(varDecmore);

				return varDecmore;
			}
		}
		syntaxError();
		return null;
	}

	// 36=28
	private TreeNode varIdmore() {
		TreeNode varIdmore = new TreeNode();
		varIdmore.setData("varIdmore");
		varIdmore.setflag(1);
		varIdmore.setNonTerminal(Enum.nonTerminals.VarIdMore);

		for (int i = 0; i < Predict.predict[37].getPredictNum(); i++) {
			if (nodeList.get(token_index).getType()
					.equals(Predict.predict[37].getPredict(i))) {
				return varIdmore;
			}
		}
		for (int i = 0; i < Predict.predict[38].getPredictNum(); i++) {
			if (nodeList.get(token_index).getType()
					.equals(Predict.predict[38].getPredict(i))) {
				match(",");
				TreeNode varIdlist = varIdlist();

				TreeNode D = new TreeNode();
				D.setData(",");
				D.setflag(0);
				D.setLength(nodeList.get(token_index).getLine());
				
				varIdmore.setChild(D);
				varIdmore.setChild(varIdlist);

				D.setFather(varIdmore);
				varIdlist.setFather(varIdmore);

				return varIdmore;
			}
		}
		syntaxError();
		return null;
	}

	// 40-1=29
	private TreeNode procDec() {
		TreeNode procDec = new TreeNode();
		procDec.setData("procDec");
		procDec.setflag(1);
		procDec.setNonTerminal(Enum.nonTerminals.ProcDec);

		for (int i = 0; i < Predict.predict[41].getPredictNum(); i++) {
			if (nodeList.get(token_index).getType()
					.equals(Predict.predict[41].getPredict(i))) {
				match("PROCEDURE");
				TreeNode varIdlist = procName();
				match("(");
				TreeNode paramList = paramList();
				match(")");
				match(";");
				TreeNode procDecPart = procDecPart();
				TreeNode procBody = procBody();
				TreeNode procDecmore = procDecmore();

				TreeNode PROCEDURE = new TreeNode();
				PROCEDURE.setData("PROCEDURE");
				PROCEDURE.setflag(0);
				PROCEDURE.setLength(nodeList.get(token_index).getLine());
				TreeNode Z = new TreeNode();
				Z.setData("(");
				Z.setflag(0);
				Z.setLength(nodeList.get(token_index).getLine());
				TreeNode Y = new TreeNode();
				Y.setData(")");
				Y.setflag(0);
				Y.setLength(nodeList.get(token_index).getLine());
				TreeNode SEMI = new TreeNode();
				SEMI.setData(";");
				SEMI.setflag(0);
				SEMI.setLength(nodeList.get(token_index).getLine());

				procDec.setChild(PROCEDURE);
				procDec.setChild(varIdlist);
				procDec.setChild(Z);
				procDec.setChild(paramList);
				procDec.setChild(Y);
				procDec.setChild(SEMI);
				procDec.setChild(procDecPart);
				procDec.setChild(procBody);
				procDec.setChild(procDecmore);

				PROCEDURE.setFather(procDec);
				varIdlist.setFather(procDec);
				Z.setFather(procDec);
				paramList.setFather(procDec);
				Y.setFather(procDec);
				SEMI.setFather(procDec);
				procDecPart.setFather(procDec);
				procBody.setFather(procDec);
				procDecmore.setFather(procDec);

				return procDec;
			}
		}
		syntaxError();
		return null;

	}

	// 41-1=30
	private TreeNode procName() {
		TreeNode procName = new TreeNode();
		procName.setData("procName");
		procName.setflag(1);
		procName.setNonTerminal(Enum.nonTerminals.ProcName);

		for (int i = 0; i < Predict.predict[44].getPredictNum(); i++) {
			if (nodeList.get(token_index).getType()
					.equals(Predict.predict[44].getPredict(i))) {
				match("ID");

				TreeNode ID = new TreeNode();
				ID.setData("ID");
				ID.setflag(0);
				ID.setLength(nodeList.get(token_index).getLine());
				
				procName.setChild(ID);

				ID.setFather(procName);

				return procName;
			}
		}
		syntaxError();
		return null;
	}

	// 41-2=31
	private TreeNode paramList() {
		TreeNode paramList = new TreeNode();
		paramList.setData("paramList");
		paramList.setflag(1);
		paramList.setNonTerminal(Enum.nonTerminals.ParamList);

		for (int i = 0; i < Predict.predict[45].getPredictNum(); i++) {
			if (nodeList.get(token_index).getType()
					.equals(Predict.predict[45].getPredict(i))) {
				return paramList;
			}
		}
		for (int i = 0; i < Predict.predict[46].getPredictNum(); i++) {
			if (nodeList.get(token_index).getType()
					.equals(Predict.predict[46].getPredict(i))) {
				TreeNode paramDeclist = paramDeclist();

				paramList.setChild(paramDeclist);

				paramDeclist.setFather(paramList);

				return paramList;
			}
		}
		syntaxError();
		return null;
	}

	// 41-3=32
	private TreeNode procDecPart() {
		TreeNode procDecPart = new TreeNode();
		procDecPart.setData("procDecPart");
		procDecPart.setflag(1);
		procDecPart.setNonTerminal(Enum.nonTerminals.ProcDecPart);

		for (int i = 0; i < Predict.predict[55].getPredictNum(); i++) {
			if (nodeList.get(token_index).getType()
					.equals(Predict.predict[55].getPredict(i))) {
				TreeNode declarePart = declarePart();

				procDecPart.setChild(declarePart);

				declarePart.setFather(procDecPart);

				return procDecPart;
			}
		}
		syntaxError();
		return null;
	}

	// 41-4=33
	private TreeNode procBody() {
		TreeNode procBody = new TreeNode();
		procBody.setData("procBody");
		procBody.setflag(1);
		procBody.setNonTerminal(Enum.nonTerminals.ProcBody);

		for (int i = 0; i < Predict.predict[56].getPredictNum(); i++) {
			if (nodeList.get(token_index).getType()
					.equals(Predict.predict[56].getPredict(i))) {
				TreeNode programBody = programBody();

				procBody.setChild(programBody);

				programBody.setFather(procBody);

				return procBody;
			}
		}
		syntaxError();
		return null;
	}

	// 41-5=34
	private TreeNode procDecmore() {
		TreeNode procDecmore = new TreeNode();
		procDecmore.setData("procDecmore");
		procDecmore.setflag(1);
		procDecmore.setNonTerminal(Enum.nonTerminals.ProcDecMore);

		for (int i = 0; i < Predict.predict[42].getPredictNum(); i++) {
			if (nodeList.get(token_index).getType()
					.equals(Predict.predict[42].getPredict(i))) {
				return procDecmore;
			}
		}
		for (int i = 0; i < Predict.predict[43].getPredictNum(); i++) {
			if (nodeList.get(token_index).getType()
					.equals(Predict.predict[43].getPredict(i))) {
				TreeNode procDec = procDec();

				procDecmore.setChild(procDec);

				procDec.setFather(procDecmore);

				return procDecmore;
			}
		}
		syntaxError();
		return null;
	}

	// 46-1=36-1
	private TreeNode paramDeclist() {
		TreeNode paramDeclist = new TreeNode();
		paramDeclist.setData("paramDeclist");
		paramDeclist.setflag(1);
		paramDeclist.setNonTerminal(Enum.nonTerminals.ParamDecList);

		for (int i = 0; i < Predict.predict[47].getPredictNum(); i++) {
			if (nodeList.get(token_index).getType()
					.equals(Predict.predict[47].getPredict(i))) {
				TreeNode param = param();
				TreeNode paramMore = paramMore();

				paramDeclist.setChild(param);
				paramDeclist.setChild(paramMore);

				param.setFather(paramDeclist);
				paramMore.setFather(paramDeclist);

				return paramDeclist;
			}
		}
		syntaxError();
		return null;
	}

	// 47-1=37-1
	private TreeNode param() {
		TreeNode param = new TreeNode();
		param.setData("param");
		param.setflag(1);
		param.setNonTerminal(Enum.nonTerminals.Param);

		for (int i = 0; i < Predict.predict[50].getPredictNum(); i++) {
			if (nodeList.get(token_index).getType()
					.equals(Predict.predict[50].getPredict(i))) {
				TreeNode typeDef = typeDef();
				TreeNode formList = formList();

				param.setChild(typeDef);
				param.setChild(formList);

				typeDef.setFather(param);
				formList.setFather(param);

				return param;
			}
		}
		for (int i = 0; i < Predict.predict[51].getPredictNum(); i++) {
			if (nodeList.get(token_index).getType()
					.equals(Predict.predict[51].getPredict(i))) {
				match("VAR");
				TreeNode typeDef = typeDef();
				TreeNode formList = formList();

				TreeNode VAR = new TreeNode();
				VAR.setData("VAR");
				VAR.setflag(0);
				VAR.setLength(nodeList.get(token_index).getLine());
				
				param.setChild(VAR);
				param.setChild(typeDef);
				param.setChild(formList);

				VAR.setFather(param);
				typeDef.setFather(param);
				formList.setFather(param);

				return param;
			}
		}
		syntaxError();
		return null;
	}

	// 47-2=38-1
	private TreeNode paramMore() {
		TreeNode paramMore = new TreeNode();
		paramMore.setData("paramMore");
		paramMore.setflag(1);
		paramMore.setNonTerminal(Enum.nonTerminals.ParamMore);

		for (int i = 0; i < Predict.predict[48].getPredictNum(); i++) {
			if (nodeList.get(token_index).getType()
					.equals(Predict.predict[48].getPredict(i))) {
				return paramMore;
			}
		}
		for (int i = 0; i < Predict.predict[49].getPredictNum(); i++) {
			if (nodeList.get(token_index).getType()
					.equals(Predict.predict[49].getPredict(i))) {
				match(";");
				TreeNode paramDeclist = paramDeclist();

				TreeNode SEMI = new TreeNode();
				SEMI.setData(";");
				SEMI.setflag(0);
				SEMI.setLength(nodeList.get(token_index).getLine());
				
				paramMore.setChild(SEMI);
				paramMore.setChild(paramDeclist);

				SEMI.setFather(paramMore);
				paramDeclist.setFather(paramMore);

				return paramMore;
			}
		}
		syntaxError();
		return null;
	}

	// 50-1=39-1
	private TreeNode formList() {
		TreeNode formList = new TreeNode();
		formList.setData("formList");
		formList.setflag(1);
		formList.setNonTerminal(Enum.nonTerminals.FormList);

		for (int i = 0; i < Predict.predict[52].getPredictNum(); i++) {
			if (nodeList.get(token_index).getType()
					.equals(Predict.predict[52].getPredict(i))) {
				match("ID");
				TreeNode fidMore = fidMore();

				TreeNode ID = new TreeNode();
				ID.setData("ID");
				ID.setflag(0);
				ID.setLength(nodeList.get(token_index).getLine());
				
				formList.setChild(ID);
				formList.setChild(fidMore);

				ID.setFather(formList);
				fidMore.setFather(formList);

				return formList;
			}
		}
		syntaxError();
		return null;

	}

	// 52-1=40-1
	private TreeNode fidMore() {
		TreeNode fidMore = new TreeNode();
		fidMore.setData("fidMore");
		fidMore.setflag(1);
		fidMore.setNonTerminal(Enum.nonTerminals.FidMore);

		for (int i = 0; i < Predict.predict[53].getPredictNum(); i++) {
			if (nodeList.get(token_index).getType()
					.equals(Predict.predict[53].getPredict(i))) {
				return fidMore;
			}
		}
		for (int i = 0; i < Predict.predict[54].getPredictNum(); i++) {
			if (nodeList.get(token_index).getType()
					.equals(Predict.predict[54].getPredict(i))) {
				match(",");
				TreeNode formList = formList();

				TreeNode D = new TreeNode();
				D.setData(",");
				D.setflag(0);
				D.setLength(nodeList.get(token_index).getLine());
				
				fidMore.setChild(D);
				fidMore.setChild(formList);

				D.setFather(fidMore);
				formList.setFather(fidMore);

				return fidMore;
			}
		}
		syntaxError();
		return null;
	}

	// 57-1=41-1
	private TreeNode stmList() {
		TreeNode stmList = new TreeNode();
		stmList.setData("stmList");
		stmList.setflag(1);
		stmList.setNonTerminal(Enum.nonTerminals.StmList);

		for (int i = 0; i < Predict.predict[58].getPredictNum(); i++) {
			if (nodeList.get(token_index).getType()
					.equals(Predict.predict[58].getPredict(i))) {
				TreeNode stm = stm();
				TreeNode stmMore = stmMore();

				stmList.setChild(stm);
				stmList.setChild(stmMore);

				stm.setFather(stmList);
				stmMore.setFather(stmList);

				return stmList;
			}
		}
		syntaxError();
		return null;
	}

	// 58-1=42-1
	private TreeNode stm() {
		TreeNode stm = new TreeNode();
		stm.setData("stm");
		stm.setflag(1);
		stm.setNonTerminal(Enum.nonTerminals.Stm);

		for (int i = 0; i < Predict.predict[61].getPredictNum(); i++) {
			if (nodeList.get(token_index).getType()
					.equals(Predict.predict[61].getPredict(i))) {
				TreeNode conditionalStm = conditionalStm();

				stm.setChild(conditionalStm);

				conditionalStm.setFather(stm);
				return stm;
			}
		}
		for (int i = 0; i < Predict.predict[62].getPredictNum(); i++) {
			if (nodeList.get(token_index).getType()
					.equals(Predict.predict[62].getPredict(i))) {
				TreeNode loopStm = loopStm();

				stm.setChild(loopStm);

				loopStm.setFather(stm);

				return stm;
			}
		}
		for (int i = 0; i < Predict.predict[63].getPredictNum(); i++) {
			if (nodeList.get(token_index).getType()
					.equals(Predict.predict[63].getPredict(i))) {
				TreeNode inputStm = inputStm();

				stm.setChild(inputStm);

				inputStm.setFather(stm);

				return stm;
			}
		}
		for (int i = 0; i < Predict.predict[64].getPredictNum(); i++) {
			if (nodeList.get(token_index).getType()
					.equals(Predict.predict[64].getPredict(i))) {
				TreeNode outputStm = outputStm();

				stm.setChild(outputStm);

				outputStm.setFather(stm);

				return stm;
			}
		}
		for (int i = 0; i < Predict.predict[65].getPredictNum(); i++) {
			if (nodeList.get(token_index).getType()
					.equals(Predict.predict[65].getPredict(i))) {
				TreeNode returnStm = returnStm();

				stm.setChild(returnStm);

				returnStm.setFather(stm);

				return stm;
			}
		}
		for (int i = 0; i < Predict.predict[66].getPredictNum(); i++) {
			if (nodeList.get(token_index).getType()
					.equals(Predict.predict[66].getPredict(i))) {
				match("ID");
				TreeNode assCall = assCall();

				TreeNode ID = new TreeNode();
				ID.setData("ID");
				ID.setflag(0);
				ID.setLength(nodeList.get(token_index).getLine());
				
				stm.setChild(ID);
				stm.setChild(assCall);

				ID.setFather(stm);
				assCall.setFather(stm);

				return stm;
			}
		}
		syntaxError();
		return null;
	}

	// 58-2=43-1
	private TreeNode stmMore() {
		TreeNode stmMore = new TreeNode();
		stmMore.setData("stmMore");
		stmMore.setflag(1);
		stmMore.setNonTerminal(Enum.nonTerminals.StmMore);

		for (int i = 0; i < Predict.predict[59].getPredictNum(); i++) {
			if (nodeList.get(token_index).getType()
					.equals(Predict.predict[59].getPredict(i))) {
				return stmMore;
			}
		}
		for (int i = 0; i < Predict.predict[60].getPredictNum(); i++) {
			if (nodeList.get(token_index).getType()
					.equals(Predict.predict[60].getPredict(i))) {
				match(";");
				TreeNode stmList = stmList();

				TreeNode SEMI = new TreeNode();
				SEMI.setData(";");
				SEMI.setflag(0);
				SEMI.setLength(nodeList.get(token_index).getLine());
				
				stmMore.setChild(SEMI);
				stmMore.setChild(stmList);

				SEMI.setFather(stmMore);
				stmList.setFather(stmMore);

				return stmMore;
			}
		}
		syntaxError();
		return null;
	}

	// 61-1=44-1
	private TreeNode conditionalStm() {
		TreeNode conditionalStm = new TreeNode();
		conditionalStm.setData("conditionalStm");
		conditionalStm.setflag(1);
		conditionalStm.setNonTerminal(Enum.nonTerminals.ConditionalStm);

		for (int i = 0; i < Predict.predict[70].getPredictNum(); i++) {
			if (nodeList.get(token_index).getType()
					.equals(Predict.predict[70].getPredict(i))) {
				match("IF");
				TreeNode relExp = relExp();
				match("THEN");
				TreeNode stmList = stmList();
				match("ELSE");
				TreeNode stmList1 = stmList();
				match("FI");

				TreeNode IF = new TreeNode();
				IF.setData("IF");
				IF.setflag(0);
				IF.setLength(nodeList.get(token_index).getLine());
				TreeNode THEN = new TreeNode();
				THEN.setData("THEN");
				THEN.setflag(0);
				THEN.setLength(nodeList.get(token_index).getLine());
				TreeNode ELSE = new TreeNode();
				ELSE.setData("ELSE");
				ELSE.setflag(0);
				ELSE.setLength(nodeList.get(token_index).getLine());
				TreeNode FI = new TreeNode();
				FI.setData("FI");
				FI.setflag(0);
				FI.setLength(nodeList.get(token_index).getLine());
				
				conditionalStm.setChild(IF);
				conditionalStm.setChild(relExp);
				conditionalStm.setChild(THEN);
				conditionalStm.setChild(stmList);
				conditionalStm.setChild(ELSE);
				conditionalStm.setChild(stmList1);
				conditionalStm.setChild(FI);

				IF.setFather(conditionalStm);
				relExp.setFather(conditionalStm);
				THEN.setFather(conditionalStm);
				stmList.setFather(conditionalStm);
				ELSE.setFather(conditionalStm);
				stmList1.setFather(conditionalStm);
				FI.setFather(conditionalStm);

				return conditionalStm;
			}
		}
		syntaxError();
		return null;

	}

	// 62-1=45-1
	private TreeNode loopStm() {
		TreeNode loopStm = new TreeNode();
		loopStm.setData("loopStm");
		loopStm.setflag(1);
		loopStm.setNonTerminal(Enum.nonTerminals.LoopStm);

		for (int i = 0; i < Predict.predict[71].getPredictNum(); i++) {
			if (nodeList.get(token_index).getType()
					.equals(Predict.predict[71].getPredict(i))) {
				match("WHILE");
				TreeNode relExp = relExp();
				match("DO");
				TreeNode stmList = stmList();
				match("ENDWH");

				TreeNode WHILE = new TreeNode();
				WHILE.setData("WHILE");
				WHILE.setflag(0);
				WHILE.setLength(nodeList.get(token_index).getLine());
				TreeNode DO = new TreeNode();
				DO.setData("DO");
				DO.setflag(0);
				DO.setLength(nodeList.get(token_index).getLine());
				TreeNode ENDWH = new TreeNode();
				ENDWH.setData("ENDWH");
				ENDWH.setflag(0);
				ENDWH.setLength(nodeList.get(token_index).getLine());

				loopStm.setChild(WHILE);
				loopStm.setChild(relExp);
				loopStm.setChild(DO);
				loopStm.setChild(stmList);
				loopStm.setChild(ENDWH);

				WHILE.setFather(loopStm);
				relExp.setFather(loopStm);
				DO.setFather(loopStm);
				stmList.setFather(loopStm);
				ENDWH.setFather(loopStm);

				return loopStm;
			}
		}
		syntaxError();
		return null;
	}

	// 63-1=46-1
	private TreeNode inputStm() {
		TreeNode inputStm = new TreeNode();
		inputStm.setData("inputStm");
		inputStm.setflag(1);
		inputStm.setNonTerminal(Enum.nonTerminals.InputStm);

		for (int i = 0; i < Predict.predict[72].getPredictNum(); i++) {
			if (nodeList.get(token_index).getType()
					.equals(Predict.predict[72].getPredict(i))) {
				match("READ");
				match("(");
				TreeNode invar = invar();
				match(")");

				TreeNode READ = new TreeNode();
				READ.setData("READ");
				READ.setflag(0);
				READ.setLength(nodeList.get(token_index).getLine());
				TreeNode Z = new TreeNode();
				Z.setData("(");
				Z.setflag(0);
				Z.setLength(nodeList.get(token_index).getLine());
				TreeNode Y = new TreeNode();
				Y.setData(")");
				Y.setflag(0);
				Y.setLength(nodeList.get(token_index).getLine());

				inputStm.setChild(READ);
				inputStm.setChild(Z);
				inputStm.setChild(invar);
				inputStm.setChild(Y);

				READ.setFather(inputStm);
				Z.setFather(inputStm);
				invar.setFather(inputStm);
				Y.setFather(inputStm);

				return inputStm;
			}
		}
		syntaxError();
		return null;
	}

	// 64-1=47-1
	private TreeNode outputStm() {
		TreeNode outputStm = new TreeNode();
		outputStm.setData("outputStm");
		outputStm.setflag(1);
		outputStm.setNonTerminal(Enum.nonTerminals.OutputStm);

		for (int i = 0; i < Predict.predict[74].getPredictNum(); i++) {
			if (nodeList.get(token_index).getType()
					.equals(Predict.predict[74].getPredict(i))) {
				match("WRITE");
				match("(");
				TreeNode exp = exp();
				match(")");

				TreeNode WRITE = new TreeNode();
				WRITE.setData("WRITE");
				WRITE.setflag(0);
				WRITE.setLength(nodeList.get(token_index).getLine());
				TreeNode Z = new TreeNode();
				Z.setData("(");
				Z.setflag(0);
				Z.setLength(nodeList.get(token_index).getLine());
				TreeNode Y = new TreeNode();
				Y.setData(")");
				Y.setflag(0);
				Y.setLength(nodeList.get(token_index).getLine());

				outputStm.setChild(WRITE);
				outputStm.setChild(Z);
				outputStm.setChild(exp);
				outputStm.setChild(Y);

				WRITE.setFather(outputStm);
				Z.setFather(outputStm);
				exp.setFather(outputStm);
				Y.setFather(outputStm);

				return outputStm;
			}
		}
		syntaxError();
		return null;
	}

	// 65-1=48-1
	private TreeNode returnStm() {
		TreeNode returnStm = new TreeNode();
		returnStm.setData("returnStm");
		returnStm.setflag(1);
		returnStm.setNonTerminal(Enum.nonTerminals.ReturnStm);

		for (int i = 0; i < Predict.predict[75].getPredictNum(); i++) {
			if (nodeList.get(token_index).getType()
					.equals(Predict.predict[75].getPredict(i))) {
				match("RETURN");

				TreeNode RETURN = new TreeNode();
				RETURN.setData("RETURN");
				RETURN.setflag(0);
				RETURN.setLength(nodeList.get(token_index).getLine());

				returnStm.setChild(RETURN);

				RETURN.setFather(returnStm);

				return returnStm;
			}
		}
		syntaxError();
		return null;
	}

	// 66-1=49-1
	private TreeNode assCall() {
		TreeNode assCall = new TreeNode();
		assCall.setData("assCall");
		assCall.setflag(1);
		assCall.setNonTerminal(Enum.nonTerminals.AssCall);

		for (int i = 0; i < Predict.predict[67].getPredictNum(); i++) {
			if (nodeList.get(token_index).getType()
					.equals(Predict.predict[67].getPredict(i))) {
				TreeNode assignmentRest = assignmentRest();

				assCall.setChild(assignmentRest);

				assignmentRest.setFather(assCall);

				return assCall;
			}
		}
		for (int i = 0; i < Predict.predict[68].getPredictNum(); i++) {
			if (nodeList.get(token_index).getType()
					.equals(Predict.predict[68].getPredict(i))) {
				TreeNode callStmrest = callStmrest();

				assCall.setChild(callStmrest);

				callStmrest.setFather(assCall);

				return assCall;
			}
		}
		syntaxError();
		return null;
	}

	// 67-1=50-1-1
	private TreeNode assignmentRest() {
		TreeNode assignmentRest = new TreeNode();
		assignmentRest.setData("assignmentRest");
		assignmentRest.setflag(1);
		assignmentRest.setNonTerminal(Enum.nonTerminals.AssignmentRest);

		for (int i = 0; i < Predict.predict[69].getPredictNum(); i++) {
			if (nodeList.get(token_index).getType()
					.equals(Predict.predict[69].getPredict(i))) {
				TreeNode variMore = variMore();
				match(":=");
				TreeNode exp = exp();

				TreeNode DD = new TreeNode();
				DD.setData(":=");
				DD.setflag(0);
				DD.setLength(nodeList.get(token_index).getLine());

				assignmentRest.setChild(variMore);
				assignmentRest.setChild(DD);
				assignmentRest.setChild(exp);

				variMore.setFather(assignmentRest);
				DD.setFather(assignmentRest);
				exp.setFather(assignmentRest);

				return assignmentRest;
			}
		}
		syntaxError();
		return null;
	}

	// 68-1=51-1
	private TreeNode callStmrest() {
		TreeNode callStmrest = new TreeNode();
		callStmrest.setData("callStmrest");
		callStmrest.setflag(1);
		callStmrest.setNonTerminal(Enum.nonTerminals.CallStmRest);

		for (int i = 0; i < Predict.predict[76].getPredictNum(); i++) {
			if (nodeList.get(token_index).getType()
					.equals(Predict.predict[76].getPredict(i))) {
				match("(");
				TreeNode actParamlist = actParamlist();
				match(")");

				TreeNode Z = new TreeNode();
				Z.setData("(");
				Z.setflag(0);
				Z.setLength(nodeList.get(token_index).getLine());
				TreeNode Y = new TreeNode();
				Y.setData(")");
				Y.setflag(0);
				Y.setLength(nodeList.get(token_index).getLine());

				callStmrest.setChild(Z);
				callStmrest.setChild(actParamlist);
				callStmrest.setChild(Y);

				Z.setFather(callStmrest);
				actParamlist.setFather(callStmrest);
				Y.setFather(callStmrest);

				return callStmrest;
			}
		}
		syntaxError();
		return null;
	}

	// 69-1=52-1
	private TreeNode variMore() {
		TreeNode variMore = new TreeNode();
		variMore.setData("variMore");
		variMore.setflag(1);
		variMore.setNonTerminal(Enum.nonTerminals.VariMore);

		for (int i = 0; i < Predict.predict[93].getPredictNum(); i++) {
			if (nodeList.get(token_index).getType()
					.equals(Predict.predict[93].getPredict(i))) {
				return variMore;
			}
		}
		for (int i = 0; i < Predict.predict[94].getPredictNum(); i++) {
			if (nodeList.get(token_index).getType()
					.equals(Predict.predict[94].getPredict(i))) {
				match("[");
				TreeNode exp = exp();
				match("]");

				TreeNode Z = new TreeNode();
				Z.setData("[");
				Z.setflag(0);
				Z.setLength(nodeList.get(token_index).getLine());
				TreeNode Y = new TreeNode();
				Y.setData("]");
				Y.setflag(0);
				Y.setLength(nodeList.get(token_index).getLine());

				variMore.setChild(Z);
				variMore.setChild(exp);
				variMore.setChild(Y);

				Z.setFather(variMore);
				exp.setFather(variMore);
				Y.setFather(variMore);

				return variMore;
			}
		}
		for (int i = 0; i < Predict.predict[95].getPredictNum(); i++) {
			if (nodeList.get(token_index).getType()
					.equals(Predict.predict[95].getPredict(i))) {
				match(".");
				TreeNode fieldVar = fieldVar();

				TreeNode D = new TreeNode();
				D.setData(".");
				D.setflag(0);
				D.setLength(nodeList.get(token_index).getLine());

				variMore.setChild(D);
				variMore.setChild(fieldVar);

				D.setFather(variMore);
				fieldVar.setFather(variMore);

				return variMore;
			}
		}
		syntaxError();
		return null;
	}

	// 69-2=53-1
	private TreeNode exp() {
		TreeNode exp = new TreeNode();
		exp.setData("exp");
		exp.setflag(1);
		exp.setNonTerminal(Enum.nonTerminals.Exp);

		for (int i = 0; i < Predict.predict[83].getPredictNum(); i++) {
			if (nodeList.get(token_index).getType()
					.equals(Predict.predict[83].getPredict(i))) {
				TreeNode term = term();
				TreeNode otherTerm = otherTerm();

				exp.setChild(term);
				exp.setChild(otherTerm);

				term.setFather(exp);
				otherTerm.setFather(exp);

				return exp;
			}
		}
		syntaxError();
		return null;
	}

	// 70=54-1
	private TreeNode relExp() {
		TreeNode relExp = new TreeNode();
		relExp.setData("relExp");
		relExp.setflag(1);
		relExp.setNonTerminal(Enum.nonTerminals.RelExp);

		for (int i = 0; i < Predict.predict[81].getPredictNum(); i++) {
			if (nodeList.get(token_index).getType()
					.equals(Predict.predict[81].getPredict(i))) {
				TreeNode exp = exp();
				TreeNode otherRele = otherRele();

				relExp.setChild(exp);
				relExp.setChild(otherRele);

				exp.setFather(relExp);
				otherRele.setFather(relExp);

				return relExp;
			}
		}
		syntaxError();
		return null;
	}

	// 72=55-1
	private TreeNode invar() {
		TreeNode invar = new TreeNode();
		invar.setData("invar");
		invar.setflag(1);
		invar.setNonTerminal(Enum.nonTerminals.Invar);

		for (int i = 0; i < Predict.predict[73].getPredictNum(); i++) {
			if (nodeList.get(token_index).getType()
					.equals(Predict.predict[73].getPredict(i))) {
				match("ID");

				TreeNode ID = new TreeNode();
				ID.setData("ID");
				ID.setflag(0);
				ID.setLength(nodeList.get(token_index).getLine());

				invar.setChild(ID);

				ID.setFather(invar);

				return invar;
			}
		}
		syntaxError();
		return null;
	}

	// 76=56-1
	private TreeNode actParamlist() {
		TreeNode actParamlist = new TreeNode();
		actParamlist.setData("actParamlist");
		actParamlist.setflag(1);
		actParamlist.setNonTerminal(Enum.nonTerminals.ActParamList);

		for (int i = 0; i < Predict.predict[77].getPredictNum(); i++) {
			if (nodeList.get(token_index).getType()
					.equals(Predict.predict[77].getPredict(i))) {
				return actParamlist;
			}
		}
		for (int i = 0; i < Predict.predict[78].getPredictNum(); i++) {
			if (nodeList.get(token_index).getType()
					.equals(Predict.predict[78].getPredict(i))) {
				TreeNode exp = exp();
				TreeNode actParammore = actParammore();

				actParamlist.setChild(exp);
				actParamlist.setChild(actParammore);

				exp.setFather(actParamlist);
				actParammore.setFather(actParamlist);

				return actParamlist;
			}
		}
		syntaxError();
		return null;
	}

	// 78=57-1
	private TreeNode actParammore() {
		TreeNode actParammore = new TreeNode();
		actParammore.setData("actParammore");
		actParammore.setflag(1);
		actParammore.setNonTerminal(Enum.nonTerminals.ActParamMore);

		for (int i = 0; i < Predict.predict[79].getPredictNum(); i++) {
			if (nodeList.get(token_index).getType()
					.equals(Predict.predict[79].getPredict(i))) {
				return actParammore;
			}
		}
		for (int i = 0; i < Predict.predict[80].getPredictNum(); i++) {
			if (nodeList.get(token_index).getType()
					.equals(Predict.predict[80].getPredict(i))) {
				match(",");
				TreeNode actParamlist = actParamlist();

				TreeNode D = new TreeNode();
				D.setData(",");
				D.setflag(0);
				D.setLength(nodeList.get(token_index).getLine());

				actParammore.setChild(D);
				actParammore.setChild(actParamlist);

				D.setFather(actParammore);
				actParamlist.setFather(actParammore);

				return actParammore;
			}
		}
		syntaxError();
		return null;
	}

	// 81=58-1
	private TreeNode otherRele() {
		TreeNode otherRele = new TreeNode();
		otherRele.setData("otherRele");
		otherRele.setflag(1);
		otherRele.setNonTerminal(Enum.nonTerminals.OtherRelE);

		for (int i = 0; i < Predict.predict[82].getPredictNum(); i++) {
			if (nodeList.get(token_index).getType()
					.equals(Predict.predict[82].getPredict(i))) {
				TreeNode cmpOp = cmpOp();
				TreeNode exp = exp();

				otherRele.setChild(cmpOp);
				otherRele.setChild(exp);

				cmpOp.setFather(otherRele);
				exp.setFather(otherRele);

				return otherRele;
			}
		}
		syntaxError();
		return null;
	}

	// 82-1=59-1
	private TreeNode cmpOp() {
		TreeNode cmpOp = new TreeNode();
		cmpOp.setData("cmpOp");
		cmpOp.setflag(1);
		cmpOp.setNonTerminal(Enum.nonTerminals.CmpOp);

		for (int i = 0; i < Predict.predict[99].getPredictNum(); i++) {
			if (nodeList.get(token_index).getType()
					.equals(Predict.predict[99].getPredict(i))) {
				match("<");

				TreeNode Y = new TreeNode();
				Y.setData("<");
				Y.setflag(0);
				Y.setLength(nodeList.get(token_index).getLine());

				cmpOp.setChild(Y);

				Y.setFather(cmpOp);

				return cmpOp;
			}
		}
		for (int i = 0; i < Predict.predict[100].getPredictNum(); i++) {
			if (nodeList.get(token_index).getType()
					.equals(Predict.predict[100].getPredict(i))) {
				match("=");

				TreeNode D = new TreeNode();
				D.setData("=");
				D.setflag(0);
				D.setLength(nodeList.get(token_index).getLine());

				cmpOp.setChild(D);

				D.setFather(cmpOp);

				return cmpOp;
			}
		}
		syntaxError();
		return null;
	}

	// 83-1=60-1
	private TreeNode term() {
		TreeNode term = new TreeNode();
		term.setData("term");
		term.setflag(1);
		term.setNonTerminal(Enum.nonTerminals.Term);

		for (int i = 0; i < Predict.predict[86].getPredictNum(); i++) {
			if (nodeList.get(token_index).getType()
					.equals(Predict.predict[86].getPredict(i))) {
				TreeNode factor = factor();
				TreeNode otherFactor = otherFactor();

				term.setChild(factor);
				term.setChild(otherFactor);

				factor.setFather(term);
				otherFactor.setFather(term);

				return term;
			}
		}
		syntaxError();
		return null;
	}

	// 83-2=61-1
	private TreeNode otherTerm() {
		TreeNode otherTerm = new TreeNode();
		otherTerm.setData("otherTerm");
		otherTerm.setflag(1);
		otherTerm.setNonTerminal(Enum.nonTerminals.OtherTerm);

		for (int i = 0; i < Predict.predict[84].getPredictNum(); i++) {
			if (nodeList.get(token_index).getType()
					.equals(Predict.predict[84].getPredict(i))) {
				return otherTerm;
			}
		}
		for (int i = 0; i < Predict.predict[85].getPredictNum(); i++) {
			if (nodeList.get(token_index).getType()
					.equals(Predict.predict[85].getPredict(i))) {
				TreeNode addOp = addOp();
				TreeNode exp = exp();

				otherTerm.setChild(addOp);
				otherTerm.setChild(exp);

				addOp.setFather(otherTerm);
				exp.setFather(otherTerm);

				return otherTerm;
			}
		}
		syntaxError();
		return null;
	}

	// 85=62-1
	private TreeNode addOp() {
		TreeNode addOp = new TreeNode();
		addOp.setData("addOp");
		addOp.setflag(1);
		addOp.setNonTerminal(Enum.nonTerminals.AddOp);

		for (int i = 0; i < Predict.predict[101].getPredictNum(); i++) {
			if (nodeList.get(token_index).getType()
					.equals(Predict.predict[101].getPredict(i))) {
				match("+");

				TreeNode ADD = new TreeNode();
				ADD.setData("+");
				ADD.setflag(0);
				ADD.setLength(nodeList.get(token_index).getLine());

				addOp.setChild(ADD);

				ADD.setFather(addOp);

				return addOp;
			}
		}
		for (int i = 0; i < Predict.predict[102].getPredictNum(); i++) {
			if (nodeList.get(token_index).getType()
					.equals(Predict.predict[102].getPredict(i))) {
				match("-");

				TreeNode SUB = new TreeNode();
				SUB.setData("-");
				SUB.setflag(0);
				SUB.setLength(nodeList.get(token_index).getLine());

				addOp.setChild(SUB);

				SUB.setFather(addOp);

				return addOp;
			}
		}
		syntaxError();
		return null;
	}

	// 86-1=63-1
	private TreeNode factor() {
		TreeNode factor = new TreeNode();
		factor.setData("factor");
		factor.setflag(1);
		factor.setNonTerminal(Enum.nonTerminals.Factor);

		for (int i = 0; i < Predict.predict[89].getPredictNum(); i++) {
			if (nodeList.get(token_index).getType()
					.equals(Predict.predict[89].getPredict(i))) {
				match("(");
				TreeNode exp = exp();
				match(")");

				TreeNode Z = new TreeNode();
				Z.setData("(");
				Z.setflag(0);
				Z.setLength(nodeList.get(token_index).getLine());
				TreeNode Y = new TreeNode();
				Y.setData(")");
				Y.setflag(0);
				Y.setLength(nodeList.get(token_index).getLine());

				factor.setChild(Z);
				factor.setChild(exp);
				factor.setChild(Y);

				Z.setFather(factor);
				exp.setFather(factor);
				Y.setFather(factor);

				return factor;
			}
		}
		for (int i = 0; i < Predict.predict[90].getPredictNum(); i++) {
			if (nodeList.get(token_index).getType()
					.equals(Predict.predict[90].getPredict(i))) {
				match("INTC");

				TreeNode INTC = new TreeNode();
				INTC.setData("INTC");
				INTC.setflag(0);
				INTC.setLength(nodeList.get(token_index).getLine());

				factor.setChild(INTC);

				INTC.setFather(factor);

				return factor;
			}
		}
		for (int i = 0; i < Predict.predict[91].getPredictNum(); i++) {
			if (nodeList.get(token_index).getType()
					.equals(Predict.predict[91].getPredict(i))) {
				TreeNode variable = variable();

				factor.setChild(variable);

				variable.setFather(factor);

				return factor;
			}
		}
		syntaxError();
		return null;
	}

	// 86-2=64-1
	private TreeNode otherFactor() {
		TreeNode otherFactor = new TreeNode();
		otherFactor.setData("otherFactor");
		otherFactor.setflag(1);
		otherFactor.setNonTerminal(Enum.nonTerminals.OtherFactor);

		for (int i = 0; i < Predict.predict[87].getPredictNum(); i++) {
			if (nodeList.get(token_index).getType()
					.equals(Predict.predict[87].getPredict(i))) {
				return otherFactor;
			}
		}
		for (int i = 0; i < Predict.predict[88].getPredictNum(); i++) {
			if (nodeList.get(token_index).getType()
					.equals(Predict.predict[88].getPredict(i))) {
				TreeNode multOp = multOp();
				TreeNode term = term();

				otherFactor.setChild(multOp);
				otherFactor.setChild(term);

				multOp.setFather(otherFactor);
				term.setFather(otherFactor);

				return otherFactor;
			}
		}
		syntaxError();
		return null;
	}

	// 88=65-1
	private TreeNode multOp() {
		TreeNode multOp = new TreeNode();
		multOp.setData("multOp");
		multOp.setflag(1);
		multOp.setNonTerminal(Enum.nonTerminals.MultOp);

		for (int i = 0; i < Predict.predict[103].getPredictNum(); i++) {
			if (nodeList.get(token_index).getType()
					.equals(Predict.predict[103].getPredict(i))) {
				match("*");

				TreeNode X = new TreeNode();
				X.setData("*");
				X.setflag(0);
				X.setLength(nodeList.get(token_index).getLine());

				multOp.setChild(X);

				X.setFather(multOp);

				return multOp;
			}
		}
		for (int i = 0; i < Predict.predict[104].getPredictNum(); i++) {
			if (nodeList.get(token_index).getType()
					.equals(Predict.predict[104].getPredict(i))) {
				match("/");

				TreeNode C = new TreeNode();
				C.setData("/");
				C.setflag(0);
				C.setLength(nodeList.get(token_index).getLine());

				multOp.setChild(C);

				C.setFather(multOp);

				return multOp;
			}
		}
		syntaxError();
		return null;
	}

	// 91=66-1
	private TreeNode variable() {
		TreeNode variable = new TreeNode();
		variable.setData("variable");
		variable.setflag(1);
		variable.setNonTerminal(Enum.nonTerminals.Variable);

		for (int i = 0; i < Predict.predict[92].getPredictNum(); i++) {
			if (nodeList.get(token_index).getType()
					.equals(Predict.predict[92].getPredict(i))) {
				match("ID");
				TreeNode variMore = variMore();

				TreeNode ID = new TreeNode();
				ID.setData("ID");
				ID.setflag(0);
				ID.setLength(nodeList.get(token_index).getLine());

				variable.setChild(ID);
				variable.setChild(variMore);

				ID.setFather(variable);
				variMore.setFather(variable);

				return variable;
			}
		}
		syntaxError();
		return null;
	}

	// 95=67-1
	private TreeNode fieldVar() {
		TreeNode fieldVar = new TreeNode();
		fieldVar.setData("fieldVar");
		fieldVar.setflag(1);
		fieldVar.setNonTerminal(Enum.nonTerminals.FieldVar);

		for (int i = 0; i < Predict.predict[96].getPredictNum(); i++) {
			if (nodeList.get(token_index).getType()
					.equals(Predict.predict[96].getPredict(i))) {
				match("ID");
				TreeNode fieldVarmore = fieldVarmore();

				TreeNode ID = new TreeNode();
				ID.setData("ID");
				ID.setflag(0);
				ID.setLength(nodeList.get(token_index).getLine());

				fieldVar.setChild(ID);
				fieldVar.setChild(fieldVarmore);

				ID.setFather(fieldVar);
				fieldVarmore.setFather(fieldVar);

				return fieldVar;
			}
		}
		syntaxError();
		return null;
	}

	// 96=68-1
	private TreeNode fieldVarmore() {
		TreeNode fieldVarmore = new TreeNode();
		fieldVarmore.setData("fieldVarmore");
		fieldVarmore.setflag(1);
		fieldVarmore.setNonTerminal(Enum.nonTerminals.FieldVarMore);

		for (int i = 0; i < Predict.predict[97].getPredictNum(); i++) {
			if (nodeList.get(token_index).getType()
					.equals(Predict.predict[97].getPredict(i))) {
				return fieldVarmore;
			}
		}
		for (int i = 0; i < Predict.predict[98].getPredictNum(); i++) {
			if (nodeList.get(token_index).getType()
					.equals(Predict.predict[98].getPredict(i))) {
				match("[");
				TreeNode exp = exp();
				match("]");

				TreeNode Z = new TreeNode();
				Z.setData("[");
				Z.setflag(0);
				Z.setLength(nodeList.get(token_index).getLine());
				TreeNode Y = new TreeNode();
				Y.setData("]");
				Y.setflag(0);
				Y.setLength(nodeList.get(token_index).getLine());

				fieldVarmore.setChild(Z);
				fieldVarmore.setChild(Y);

				Z.setFather(fieldVarmore);
				Y.setFather(fieldVarmore);

				return fieldVarmore;
			}
		}
		syntaxError();
		return null;
	}

	// 匹配字符
	private void match(String string) {
		token_index++;
	}

	// 错误信息的输出
	private void syntaxError() {
		System.out.println("在第" + nodeList.get(token_index).getLine() + "行第"
				+ nodeList.get(token_index).getRow() + "列出现了语法错误！");
		System.exit(0);
	}

	public static void main(String[] args) {
		new DrawTree().drawtree(new RecursiveDescent().program());
	}

}
