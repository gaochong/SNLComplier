package parser;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;

import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JScrollPane;

import common.TreeNode;

public class DrawTreeDialog extends JDialog {


	static int X;
	static int Space = 30;
	static int Width, High;
	static NewPanel1 Panel;



	public DrawTreeDialog(JFrame parent, TreeNode root) {
		super(parent, "语法树", true);
		X = 20;
		High = getTreeInf(root, 20);
		Width = X;
		Panel.Root = root;
		//setVisible(true);
		Panel = new NewPanel1();
		Panel.setPreferredSize(new Dimension(Width, High));
		JScrollPane pane = new JScrollPane(Panel);
		add(pane);
		setSize(1200, 700);
	}

	public static int getTreeInf(TreeNode root, int Y) {
		int temp, Length, width = 0;
		String str = "";

		if (root.getflag() == 0 || (root.getchildNum() == 0)) {
			//叶子节点
			if (root.getflag() == 0)
				str += root.getData();
			else
				str += root.getNonTerminal();
			Length = str.length() * 8;
			width = Length + Space; //设置文字的宽度
			root.setLength(Length);
			root.setWidth(width);
			root.setX(X);
			root.setY(Y);
			X += width;
		} else {
			//非叶子节点
			str += root.getNonTerminal();
			Length = str.length() * 8;
			root.setLength(Length);
			root.setY(Y);
			temp = X;
			for (int i = 0; i < root.getchildNum(); i++) {
				//递归
				width += getTreeInf(root.getChild(i), Y + 150);
			}
			root.setX(temp + width / 2 - Length / 2);
			if (width < Length) {
				width = Length / 2 + width / 2;
				X += Length - width + Space;
			}
			root.setWidth(width);

		}
		return width;
	}

	@SuppressWarnings("static-access")
	public static void drawtree(TreeNode root) {
//		X = 20;
//		High = getTreeInf(root, 20);
//		Width = X;
//		Panel.Root = root;
//
//		DrawTreeDialog frame = new DrawTreeDialog(null);
//		frame.setTitle("语法分析树");
//		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
//		frame.setSize(800, 600);
//		frame.setVisible(true);
	}
}

class NewPanel1 extends JLabel {

	private static final long serialVersionUID = 7303234159560655659L;
	public static TreeNode Root;

	public void draw(TreeNode root, Graphics g) {
		g.drawRect(root.getX() - 3, root.getY() - 15, root.getLength() + 1, 20);
		if (root.getflag() == 0) {
			//终级符
			g.drawString(root.getData(), root.getX(), root.getY());
		} else {
			//非终级符
			g.drawString("" + root.getNonTerminal(), root.getX(), root.getY());
			if (root.getchildNum() != 0) {
				for (int i = 0; i < root.getchildNum(); i++) {
					g.drawLine(root.getX() + root.getLength() / 2,
							root.getY() + 5, root.getChild(i).getX()
									+ root.getChild(i).getLength() / 2, root
									.getChild(i).getY() - 15);
					draw(root.getChild(i), g);
				}
			}
		}
	}

	public void paint(Graphics g) {
		super.paint(g);
		g.setColor(Color.white);
		g.fillRect(0, 0, this.getWidth(), this.getHeight());
		g.setColor(Color.black);
		draw(Root, g);
	}
}
