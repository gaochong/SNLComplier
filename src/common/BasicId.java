package common;

/**
 * 标识符基类结构
 * @author Administrator
 *
 */
public abstract class BasicId {
	public String data;
	public InnerType itype;
	public Enum.idKind kind;
	public int line;
}
