package common;

public interface Enum {
	/**
	 * 字符类别
	 * 
	 * @author Chong
	 *
	 */
	public enum charType {
		LETTER, DIGIT, COLON, LLARPAREN, DOT, CHARC, EQ, SEMI, PLUS, MINUS, TIMES, OVER, LPAREN, RPAREN, LMIDPAREN, RMIDPAREN, ENDFILE, SPACE, COMMA, LT;
	}

	/**
	 * 状态类型
	 * 
	 * @author Chong
	 *
	 */
	public enum stateType {
		S0, S1, S2, S3, S4, S5, S6, S7, S8, S9, S10, S11, S12, ID, INTC, CHAR, ASSIGN, INCOMMENT, INRANGE, SPECIAL,  ERROR;
	}

	/**
	 * 单词分类
	 * 
	 * @author Chong
	 *
	 */
	public enum lexType {

		/* 保留字 */
		PROGRAM, PROCEDURE, TYPE, VAR, IF, THEN, ELSE, FI, WHILE, DO, ENDWH, BEGIN, END, READ, WRITE, ARRAY, OF, RECORD, RETURN,

		INTEGER, CHAR,
		/* 多字符单词符号 */
		ID, INTC, CHARC,
		/* 特殊符号 */
		ASSIGN, EQ, LT, PLUS, MINUS, TIMES, OVER, LPAREN, RPAREN, DOT, COLON, SEMI, COMMA, LMIDPAREN, RMIDPAREN, UNDERANGE,
		/* 簿记单词符号 */
		ENDFILE, ERROR;
	}

	/*
	 * 非终极符 67个
	 */
	public enum nonTerminals {
		/* 总程序: 1 */
		Program,
		/* 程序头 :2 */
		ProgramHead, ProgramName,
		/* 程序声明 :1 */
		DeclarePart,
		/* 类型声明: 5 */
		TypeDecpart, TypeDec, TypeDecList, TypeDecMore, TypeId,
		/* 类型 :11 */
		TypeDef, BaseType, StructureType, ArrayType, Low, Top, RecType, FieldDecList, FieldDecMore, IdList, IdMore,
		/* 变量声明 :6 */
		VarDecpart, VarDec, VarDecList, VarDecMore, VarIdList, VarIdMore,
		/* 过程声明 :4 */
		ProcDecpart, ProcDec, ProcDecMore, ProcName,
		/* 参数声明:6 ProcDeclaration, */
		ParamList, ParamDecList, ParamMore, Param, FormList, FidMore,
		/* 过程中的声明部分:1 */
		ProcDecPart,
		/* 过程体 :1 */
		ProcBody,
		/* 主程序体 :1 */
		ProgramBody,
		/* 语句序列:2 */
		StmList, StmMore,
		/* 语句 :2 */
		Stm, AssCall,
		/* 赋值语句:1 */
		AssignmentRest,
		/* 条件:1 */
		ConditionalStm,
		/* 循环 :1 */
		LoopStm,
		/* 输入:2 */
		InputStm, Invar,
		/* 输出:1 */
		OutputStm,
		/* 返回:1 */
		ReturnStm,
		/* 过程调用:3 */
		CallStmRest, ActParamList, ActParamMore,
		/* 条件表达式:2 */
		RelExp, OtherRelE,
		/* 算术表达式:2 */
		Exp, OtherTerm,
		/* 项:2 */
		Term, OtherFactor,
		/* 因子 :8 */
		Factor, Variable, VariMore, FieldVar, FieldVarMore, CmpOp, AddOp, MultOp;
	}

	/**
	 * 节点类型
	 * 
	 * @author Chong
	 *
	 */
	public enum nodeKind {
		ProK, PheadK, TypeK, VarK, ProcDecK, StmLK, DecK, StmtK, ExpL;
	}

	/**
	 * 数据的内部类型
	 * 
	 * @author Administrator
	 *
	 */
	public enum innerType {
		IntType, CharType, ArrayType, RecordType, BoolType;
	}

	/**
	 * 标示符类别
	 * 
	 * @author Administrator
	 *
	 */
	public enum idKind {
		typeKind, varKind, procKind, specKind;
	}

	/**
	 * 变量访问类型
	 * 
	 * @author Administrator
	 *
	 */
	public enum accessType {
		dir, indir;
	}

	public enum decKind {
		ArrayK, CharK, IntegerK, RecordK, IdK;
	}

	public enum stmtKind {
		IfK, WhileK, AssignK, ReadK, WriteK, CallK, ReturnK;
	}

	public enum expKind {
		OpK, ConstK, VariK;
	}

	public enum varKind {
		IdV, ArrayMembV, FieldMembV;
	}

	public enum expType {
		Void, Integer, Boolean;
	}

	public enum paramType {
		ValParamType, VarparamType;
	}
}