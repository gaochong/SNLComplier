package common;

import java.util.ArrayList;

/**
 * 记录类型的内部表示结构
 * @author Administrator
 *
 */
public class InnerRecordType extends InnerType {
	/**
	 * 记录的域类型
	 * @author Administrator
	 *
	 */
	public class fieldChain {
		public String idname;
		public InnerType unitType;
		public int offset;
	}
	//结构体中各域组成的数组
	public ArrayList<fieldChain> body;
}
