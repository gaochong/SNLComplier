package common;

/**
 * 数组类型的内部表示结构
 * @author Administrator
 *
 */
public class InnerArrayType extends InnerType {
	public Enum.innerType indexTy;
	public int low;
	public int up;
	public InnerType elementTy;
}
