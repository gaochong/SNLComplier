package common;
/**
 * 过程标识符
 * @author Administrator
 *
 */
public class ProcId extends BasicId {
	public int level;
	public int paramCount;//参数个数
	public int size;
	public int code;
	public int paramStart;//参数开始的位置
}
