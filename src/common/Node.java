package common;

import common.Enum.*;

/**
 * Token节点结构
 * @author Chong
 *
 */
public class Node {
	String Data ; //数据
	int    Line , Row ;//行号, 列号
	lexType    type ;// 单词类型
	public Node(){
	}
	public String getData() {
		return Data;
	}
	public void setData(String data) {
		Data = data;
	}
	public int getLine() {
		return Line;
	}
	public void setLine(int line) {
		Line = line;
	}
	public int getRow() {
		return Row;
	}
	public void setRow(int row) {
		Row = row;
	}
	public lexType getType() {
		return type;
	}
	public void setType(lexType integer) {
		this.type = integer;
	}
}