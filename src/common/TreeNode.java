package common;

/**
 * 语法树结构
 * @author Chong
 *
 */
public  class TreeNode {
	
	int childNum ; //孩子个数, 不超过10个 
	TreeNode[] child = new TreeNode[10] ; //孩子
	TreeNode father ; //父亲节点
	
	int flag ;     // 0 表示叶子节点
	Enum.nonTerminals NonTerminal ; //非终级符
	Enum.lexType Terminal ; //终级符
	int line;
	public int getLine() {
		return line;
	}

	public void setLine(int line) {
		this.line = line;
	}
	String data ; //内容
	
	int x , y , width , length ; //显示的位置及大小
	
	public TreeNode() {
		childNum = 0 ;
	}
	
	public void setChild( TreeNode node ) {
		child[childNum] = new TreeNode() ;
		child[childNum] = node ;
		childNum ++ ;
	}
	
	public void setFather( TreeNode node ) {
		father = new TreeNode() ;
		father = node ;
	}
	
	public void setflag( int FLAG ) {
		flag = FLAG ;
	}
	
	public void setData( String DATA ) {
		data = DATA ; 
	}
	
	public void setNonTerminal( Enum.nonTerminals nonTerminal ) {
		NonTerminal = nonTerminal ;
	}
	
	public void setTerminal( Enum.lexType terminal ) {
		Terminal = terminal ;
	}
	
	public int getchildNum() {
		return childNum ;
	}
	
	public TreeNode getChild( int num ) {
		return child[num] ;
	}
	
	public TreeNode getFather() {
		return father ;
	}
	
	public int getflag() {
		return flag ; 
	}
	
	public Enum.nonTerminals getNonTerminal() {
		return NonTerminal ; 
	}
	
	public Enum.lexType getTerminal() {
		return Terminal ; 
	}
	
	public String getData() {
		return data ; 
	}
	
	public void setX( int X ) {
		x = X ;
	}
	public int  getX() {
		return x ;
	}
	
	public void setY( int Y ) {
		y = Y ;
	}
	public int  getY() {
		return y ; 
	}
	
	public void setWidth( int Width ) {
		width = Width ;
	}
	public int  getWidth() {
		return width ;
	}
	
	public void setLength( int Length ) {
		length = Length ;
	}
	public int  getLength() {
		return length ;
	}
	
}


