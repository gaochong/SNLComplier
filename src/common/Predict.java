package common;

/**
 * predict集的结构
 * @author Chong
 *
 */
public class Predict {
	int         predictNum ; //产生式包含单词的类别数
	Enum.lexType[] predict = new Enum.lexType[20] ;//predict集
	
	public Predict() {
		predictNum = 0 ; 
	}
	
	public void setPredict( Enum.lexType pre ) {
		predict[predictNum] = pre ;
		predictNum ++ ;
	}
	
	public int getPredictNum() {
		return predictNum ;
	}
	
	public Enum.lexType getPredict( int number ) {
		return predict[number] ;
	}
}
