package common;

/**
 * 产生式的结构
 * 
 * @author Chong
 *
 */
public class Production {
	Enum.nonTerminals Head;// 产生式左部
	int productNum; // 产生式右部包含子部个数

	// 内部类
	public class product {
		public int flag; // 0表示终极符，1表示非终极符
		public Enum.nonTerminals nonterminals;
		public Enum.lexType terminals;
	}

	/**
	 * 产生式右部
	 */
	public product[] Product = new product[10];

	public Production() {
		productNum = 0;
	}

	public void setHead(Enum.nonTerminals head) {
		Head = head;
	}

	// 添加一个非终级符
	public void setProduction(Enum.nonTerminals nonterminal) {
		Product[productNum] = new product();
		Product[productNum].flag = 1;
		Product[productNum].nonterminals = nonterminal;
		productNum++;
	}

	// 添加一个终级符
	public void setProduction(Enum.lexType terminal) {
		Product[productNum] = new product();
		Product[productNum].flag = 0;
		Product[productNum].terminals = terminal;
		productNum++;
	}

	public Enum.nonTerminals getHead() {
		return Head;
	}

	public int getproductNum() {
		return productNum;
	}

	/**
	 * 获取第number个子部的类型
	 * 
	 * @param number
	 * @return
	 */
	public int getflag(int number) {
		if (Product[number].flag == 1)
			return 1;
		else
			return 0;
	}

	public Enum.nonTerminals getProductNonterminal(int number) {
		return Product[number].nonterminals;
	}

	public Enum.lexType getProductTerminal(int number) {
		return Product[number].terminals;
	}
};