package common;

/**
 * 错误信息结构
 * @author Chong
 *
 */
public class Error {
	static int  Line ; //行
	static int Row;//列
	static int  ErrorType  ; //错误类型1,2,...
	static boolean  Flag ; //错误标志
	public Error(){
		Line = 0 ; 
		Row  = 0 ;
		ErrorType = 0 ; 
		Flag = false ;
	}
	/**
	 * 打印错误信息
	 */
	public static void printError(String info) {
		if( ErrorType == 1 ) System.out.print( "词法错误" ) ; 
		else if( ErrorType == 2 ) System.out.print( "语法错误" ) ;
		else if(ErrorType == 3) System.out.print(info+" 语义错误");
		
		System.out.println( "  行:" + Line + "  列: " + Row  ) ; 
		System.exit(0);
	}
	public static void printError() {
		if( ErrorType == 1 ) System.out.print( "词法错误" ) ; 
		else if( ErrorType == 2 ) System.out.print( "语法错误" ) ;
		else if(ErrorType == 3) System.out.print("语义错误");
		
		System.out.println( "  行:" + Line + "  列: " + Row  ) ; 
		System.exit(0);
	}
	public static void setLine( int line ) {
		Line = line ; 
	}
	public static void setRow( int row ) {
		Row = row ; 
	}
	public static void setErrorType( int type ) {
		ErrorType = type ;
	}
	public static void setFlag( boolean flag ) {
		Flag = flag ; 
	}
	public static int getLine() {
		return Line ;
	}
	public static int getRow()  {
		return Row  ;
	}
	public static int getErrorType() {
		return ErrorType ;
	}
	public static boolean getFlag() {
		return Flag ; 
	}
	/**
	 * 设置错误信息
	 * @param line
	 * @param row
	 * @param type
	 */
	public static void setError( int line , int row , int type ) {
		Line = line ; 
		Row  = row  ;
		ErrorType = type ;
		Flag = true    ;
	}
}
